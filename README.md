<!-- GETTING STARTED -->
## Getting Started

### General info
ContactList app

### Prerequisites

* Node js
```sh
https://nodejs.org/dist/v12.15.0/node-v12.15.0-x64.msi
```

### Installation

1. Clone this repo
```sh
git clone https://gitlab.com/krzosdominik/contactlist.git
```
2. Install NPM packages
```sh
npm install
```
3. Run app
```sh
npm start
```
