import React, { useState } from "react";

const FilterInput = ({ onInputChange }) => {
    const [inputvalue, setInputValue] = useState("");

    const handleInputChange = ({ target }) => {
        setInputValue(target.value);
        onInputChange(target.value);
    };

    return (
        <div className="form-group">
            <input
                type="text"
                className="form-control"
                placeholder="Enter first or last name"
                value={inputvalue}
                onChange={handleInputChange}
            />
        </div>
    )
};

export default FilterInput;