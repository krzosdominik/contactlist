import React from 'react';

import Header from './Header';
import Content from './Content';

const App = () => {
  return (
    <>
      <Header header="Contact" />
      <div className="container">
        <Content />
      </div>
    </>
  );
}

export default App;
