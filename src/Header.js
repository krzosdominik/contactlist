import React from 'react';

const Header = ({ header }) => {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light text-center">
            <div className="container">
                <a className="navbar-brand" href="/">{header}</a>
            </div>
        </nav>
    )
};

export default Header;