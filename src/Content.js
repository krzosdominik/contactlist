import React, { useState, useEffect } from 'react';
import axios from 'axios';

import FilterInput from './FilterInput';
import ContactsList from './ContactsList';

const API_URL = 'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com';

const ContactList = () => {
    const [contacts, setContacts] = useState([]);
    const [filteredContacts, setFilteredContacts] = useState([]);
    const [inputvalue, setInputValue] = useState("");

    const sortAndSetState = data => {
        data.sort((a, b) => a.last_name.localeCompare(b.last_name));
        setContacts(data);
        setFilteredContacts(data);
    };

    useEffect(() => {
        axios.get(`${API_URL}/users.json`)
            .then(response => response.data)
            .then(data => sortAndSetState(data))
            .catch(error => console.log(error));
    }, []);

    useEffect(() => {
        if (inputvalue) {
            const filteredByName = contacts
                .filter(({ first_name, last_name }) => {
                    return first_name.toLowerCase().includes(inputvalue) || last_name.toLowerCase().includes(inputvalue);
                });
            setFilteredContacts(filteredByName);
        } else {
            setFilteredContacts(contacts);
        }

    }, [contacts, inputvalue]);

    const handleInputChange = value => setInputValue(value);

    return (
        <>
            <FilterInput onInputChange={handleInputChange} />
            <ContactsList contacts={filteredContacts} />
        </>
    );
};

export default ContactList;