import React, { useState, useEffect } from 'react';

import ListItem from './ListItem';

const ContactsList = ({ contacts }) => {
    const [checkedContacts, setCheckedContacts] = useState([]);

    useEffect(() => {
        checkedContacts.length
            ? console.log(checkedContacts)
            : console.log("Select contacts");
    }, [checkedContacts]);

    const handleContactClick = id => {
        checkedContacts.includes(id)
            ? setCheckedContacts(checkedContacts.filter(item => item !== id))
            : setCheckedContacts([...checkedContacts, id]);
    };

    return (
        <ul className="list-group">
            {contacts.map(({ id, first_name, last_name, email, avatar }) => (
                <ListItem
                    key={id}
                    onContactClick={handleContactClick}
                    checkedContacts={checkedContacts}
                    id={id}
                    firstName={first_name}
                    lastName={last_name}
                    email={email}
                    avatar={avatar}
                />
            ))}
        </ul>
    )
};

export default ContactsList;