import React from 'react';

const ListItem = ({ onContactClick, checkedContacts, id, firstName, lastName, email, avatar }) => {

    const handleContactClick = () => onContactClick(id);

    const handleInputChange = () => onContactClick(id);

    return (
        <li
            className="list-group-item d-flex justify-content-between align-items-center"
            onClick={handleContactClick}
        >
            <div className="d-flex">
                <figure className="mr-4">
                    <img
                        className="border rounded-circle"
                        src={avatar}
                        alt={`${firstName} ${lastName}`}
                    />
                </figure>
                <div className="text-left">
                    <h3>{firstName} {lastName}</h3>
                    <small>{email}</small>
                </div>
            </div>
            <div className="input-group-text">
                <input
                    type="checkbox"
                    name={id}
                    value={id}
                    checked={checkedContacts.includes(id)}
                    onChange={handleInputChange}
                />
            </div>
        </li>
    )
};

export default ListItem;